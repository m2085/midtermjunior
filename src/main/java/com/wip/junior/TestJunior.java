/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.junior;

/**
 *
 * @author WIP
 */
public class TestJunior {
    public static void main(String[] args) {
        Junior Kla = new Junior("Kla");
        System.out.println(Kla.pickSnack("2"));
        
        Junior Mana = new Junior("Mana");
        System.out.println(Mana.pickSnack());
        
        Junior Manee = new Junior("Manee");
        System.out.println(Manee.pickSnack("1", "2"));
        
        Junior Pete = new Junior("Pete");
        System.out.println(Pete.pickSnack("5"));
        
        Junior Chujai = new Junior("Chujai");
        System.out.println(Chujai.pickSnack("4", "7"));
        
    }
}
