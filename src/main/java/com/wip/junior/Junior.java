/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.junior;

/**
 *
 * @author WIP
 */
public class Junior {

    private final String name;
    private String snack, snack2;
    private int num;

    Junior(String name) {
        this.name = name;
        System.out.println("--create " + this.name + "--");
    }

    public String pickSnack() {
        return JuniorPick();
    }

    public String pickSnack(String snack) {
        this.snack = snack;
        Distribute(snack);
        if (num == 1 || num == 2 || num == 3 || num == 4) {
            return toString() + this.snack;
        } else {
            return JuniorDontPick();
        }
    }

    public String pickSnack(String snack, String snack2) {
        this.snack = snack;
        this.snack2 = snack2;
        Distribute(this.snack);
        if (num == 1 || num == 2 || num == 3 || num == 4) {
            Distribute(this.snack2);
            if (num == 1 || num == 2 || num == 3 || num == 4) {
                return toString() + this.snack + " " + this.snack2;
            } else {
                return toString() + this.snack;
            }

        } else {
            this.snack2 = snack2;
            Distribute(this.snack2);
            if (num == 1 || num == 2 || num == 3 || num == 4) {
                return toString() + this.snack2;
            } else {
                return JuniorDontPick();
            }
        }
    }

    //DistributeAttribute
    public int Distribute(String snack) {
        num = 0;
        switch (snack) {
            case "1":
                num = 1;
                break;
            case "2":
                num = 2;
                break;
            case "3":
                num = 3;
                break;
            case "4":
                num = 4;
                break;
            default:
                break;
        }
        return 0;
    }

    public String JuniorDontPick() {
        return "none of this";
    }

    public String JuniorPick() {
        return this.name + " not specified, but picked it";
    }

    @Override
    public String toString() {
        return this.name + " have type is ";
    }

}
